<?php
function tentukan_deret_geometri($arr) {
    $temp = array();
    $result = "";
    foreach ($arr as $key=>$nilai) {
        if ($key>0){
            array_push($temp,$nilai/$arr[$key-1]);
            if ($key-1>0){
                if ($temp[$key-1]!=$temp[$key-2]){
                    $result = "False";
                    break;
                }
                else{
                    $result = "True";
                }
            }
        }
    }
    return $result;
}
echo tentukan_deret_geometri([1, 3, 9, 27, 81])."<br>"; // true
echo tentukan_deret_geometri([2, 4, 8, 16, 32])."<br>"; // true
echo tentukan_deret_geometri([2, 4, 6, 8])."<br>"; // false
echo tentukan_deret_geometri([2, 6, 18, 54])."<br>"; // true
echo tentukan_deret_geometri([1, 2, 3, 4, 7, 9])."<br>"; //false