<?php
function tentukan_deret_aritmatika($arr) {
    $temp = array();
    $result = "";
    foreach ($arr as $key=>$nilai) {
        if ($key>0){
            array_push($temp,$nilai-$arr[$key-1]);
            if ($key-1>0){
                if ($temp[$key-1]!=$temp[$key-2]){
                    $result = "False";
                    break;
                }
                else{
                    $result = "True";
                }
            }
        }
    }
    return $result;
}
echo tentukan_deret_aritmatika([1, 2, 3, 4, 5, 6])."<br>";// true
echo tentukan_deret_aritmatika([2, 4, 6, 12, 24])."<br>";// false
echo tentukan_deret_aritmatika([2, 4, 6, 8])."<br>"; //true
echo tentukan_deret_aritmatika([2, 6, 18, 54])."<br>";// false
echo tentukan_deret_aritmatika([1, 2, 3, 4, 7, 9])."<br>";// false