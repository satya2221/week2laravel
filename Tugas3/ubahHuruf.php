<?php
function ubah_huruf($string){
    $string=strtolower($string);
    $huruf = range('a','z');
    $huruf_baru = range('b','z');
    array_push($huruf_baru,'a');

    $index_kata = array();
    for($i=0;$i<strlen($string);$i++){
        for($j=0;$j<sizeof($huruf_baru);$j++){
            if ($string[$i]==$huruf[$j]){
                array_push($index_kata,$j);
            }
        }
    }
    $final = "";
    foreach ($index_kata as $item) {
        $final = $final.$huruf_baru[$item];
    }
    return $final;
}
echo ubah_huruf('wow')."<br>"; // xpx
echo ubah_huruf('developer')."<br>"; // efwfmpqfs
echo ubah_huruf('laravel')."<br>"; // mbsbwfm
echo ubah_huruf('keren')."<br>"; // lfsfo
echo ubah_huruf('semangat')."<br>"; // tfnbohbu
