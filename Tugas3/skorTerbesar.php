<?php
function skor_terbesar($arr){
    $terbesar = array();
    for($i=0;$i<sizeof($arr);$i++){
        for ($j=0;$j<sizeof($arr);$j++) {
            echo "j = ".$arr[$j]["kelas"]."<br>";
            echo "j+1 = ".$arr[$i+1]["kelas"]."<br>";
            if ($arr[$j]["kelas"] == $arr[$i + 1]["kelas"]) {
                if ($arr[$j]["nilai"] > $arr[$i + 1]["nilai"]) {
                    array_push($terbesar, array("nama" => $arr[$j]["nama"], "kelas" => $arr[$j]["kelas"], "nilai" => $arr[$j]["nilai"]));
                }
                elseif($arr[$j]["nilai"] < $arr[$i + 1]["nilai"]) {
                    array_push($terbesar, array("nama" => $arr[$i + 1]["nama"], "kelas" => $arr[$i+1]["kelas"], "nilai" => $arr[$i+1]["nilai"]));
                }
                break;
            }

        }
        if ($i+1==sizeof($arr)-1){
            array_push($terbesar, array("nama" => $arr[$i]["nama"], "kelas" => $arr[$i]["kelas"], "nilai" => $arr[$i]["nilai"]));
            break;
        }
        echo "<br>";
    }
    return $terbesar;
}
$skor = [
    [
        "nama" => "Bobby",
        "kelas" => "Laravel",
        "nilai" => 78
    ],
    [
        "nama" => "Regi",
        "kelas" => "React Native",
        "nilai" => 86
    ],
    [
        "nama" => "Aghnat",
        "kelas" => "Laravel",
        "nilai" => 90
    ],
    [
        "nama" => "Indra",
        "kelas" => "React JS",
        "nilai" => 85
    ],
    [
        "nama" => "Yoga",
        "kelas" => "React Native",
        "nilai" => 77
    ],
];
print_r(skor_terbesar($skor));