<?php


class animal
{
    private $namanya;
    private $kakinya = 2;
    private $cold_blood = false;
    public function __construct($nama)
    {
        $this->namanya = $nama;
    }

    /**
     * @return mixed
     */
    public function getNamanya()
    {
        return $this->namanya;
    }
    public function getLegs(){
        return $this->kakinya;
    }
    public function getBlood(){
        return $this->cold_blood;
    }
}