<?php
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");
echo $sheep->getNamanya()."<br>";
echo $sheep->getLegs()."<br>";
echo $sheep->getBlood()? 'true' : 'false'."<br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell();// "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"