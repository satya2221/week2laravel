<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
<h1>Berlatih Array</h1>

<?php
echo "<h3> Soal 1 </h3>";
/*
    SOAL NO 1
    Kelompokkan nama-nama di bawah ini ke dalam Array.
    Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"
    Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
*/
$kids = array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"); // Lengkapi di sini
$adults = array("Hopper", "Nancy",  "Joyce", "Jonathan", "Murray");
echo "<h3> Soal 2</h3>";
/*
    SOAL NO 2
    Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
*/
echo "Cast Stranger Things: ";
echo "<br>";
echo "Total Kids: ". count($kids) ; // Berapa panjang array kids
echo "<br>";
echo "<ol>";
foreach ($kids as $kid) {
    echo "<li> $kid </li>";
}
// Lanjutkan

echo "</ol>";

echo "Total Adults: ".count($adults) ;// Berapa panjang array adults
echo "<br>";
echo "<ol>";
foreach ($adults as $adult) {
    echo "<li> $adult </li>";
}
// Lanjutkan

echo "</ol>";

/*
    SOAL No 3
    Susun data-data berikut ke dalam bentuk Asosiatif Array (Array Multidimensi)

    Name: "Will Byers"
    Age: 12,
    Aliases: "Will the Wise"
    Status: "Alive"

    Name: "Mike Wheeler"
    Age: 12,
    Aliases: "Dungeon Master"
    Status: "Alive"

    Name: "Jim Hopper"
    Age: 43,
    Aliases: "Chief Hopper"
    Status: "Deceased"

    Name: "Eleven"
    Age: 12,
    Aliases: "El"
    Status: "Alive"

*/
$bio = array(
           array("Will Byers","12","Will the Wise","Alive"),
           array("Mike Wheeler","12","Dungeon Master","Alive"),
           array("Jim Hopper","43","Chief Hopper","Deceased"),
           array("Eleven","12","El","Alive"),
       );
$angkane = [70,88,90];
$angkane[] = 100;
print_r($angkane);
$array = array(0 => 'blue', 1 => 'red', 2 => 'green', 3 => 'red');

$key = array_search('green', $array); // $key = 2;
$key = array_search('red', $array);   // $key = 1;
echo $key."<br>";

$string = "102*2";
$simbol = array('*','/','+','-','%');
$index = 0;
for($i=0;$i<strlen($string);$i++){
    if (gettype(array_search($string[$i],$simbol))=="integer"){
        $indexnya = array_search($string[$i],$simbol);
        $simbolnya = $simbol[array_search($string[$i],$simbol)];
        break;
    }
    $index++;
}
echo $index."<br>";
echo $simbolnya."<br>";
$angka1 = "";
for ($i=0;$i<$index;$i++){
    $angka1 = $angka1.$string[$i];
}
$angka1 = intval($angka1);
echo $angka1."<br>";
$angka2 = "";
for ($i=$indexnya+1;$i<strlen($string);$i++){
    $angka2 = $angka2.$string[$i];
}
$angka2 = intval($angka2);
if ($simbolnya=='*'){
    $hasil = $angka1 * $angka2;
}
elseif ($simbolnya=='/'){
    $hasil = $angka1 / $angka2;
}
elseif ($simbolnya=='+'){
    $hasil = $angka1 + $angka2;
}
elseif ($simbolnya=='-'){
    $hasil = $angka1 - $angka2;
}
elseif ($simbolnya=='%'){
    $hasil = $angka1 % $angka2;
}
echo $hasil;
?>
</body>
</html>