1. Buat database myshop
CREATE DATABASE myshop;

2. Buat tabel users, items, categories
-  CREATE TABLE users (id int NOT NULL AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255), PRIMARY KEY (id) );
-  CREATE TABLE categories (id int NOT NULL AUTO_INCREMENT, name varchar(255), PRIMARY KEY (id));
-  CREATE TABLE items (id int NOT NULL AUTO_INCREMENT, name varchar(255), description varchar(255), price int, stock int, category_id int, PRIMARY KEY (id), FOREIGN KEY (category_id) REFERENCES categories(id));

3. Isi tabelnya
-  INSERT INTO users(name, email, password) VALUES ('John Doe', 'john@doe.com', 'john123'), ('Jane Doe', 'jane@doe.com', 'jenita123');
-  INSERT INTO categories(name) VALUES ('gadget'),('cloth'), ('men'), ('women'), ('branded');
-  INSERT INTO items(name, description, price, stock, category_id) VALUES
    -> ('Sumsang B50', 'hape keren dari merek sumsang', 4000000, 100, 1),
    -> ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
    -> ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

4. Ambil data dari database
a) Ambil data users
-  SELECT id, name, email FROM users;
b) Ambil data items
-  SELECT * FROM items WHERE price > 1000000;
-  SELECT * FROM items WHERE name LIKE '%watch%';
c) Joinss
-  SELECT * FROM items INNER JOIN categories ON items.category_id = categories.id;

5. Update hp sumsang
-  UPDATE items SET price = 2500000 WHERE name LIKE 'Sumsang%';